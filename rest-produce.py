import json
import time
import requests
from requests.exceptions import RequestException

from projectsecrets import KAFKA_TGV_API_KEY, KAFKA_TGV_API_PASSWORD, KAFKA_TGV_BOOTSTRAP_SERVERS, KAFKA_TGV_SCHEMA_REGISTRY_URL, KAFKA_TGV_SCHEMA_REGISTRY_USERNAME, KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD, KAFKA_TGV_REST_PROXY_URL, KAFKA_TGV_CLUSTER, KAFKA_TGV_CONSUMER_GROUP, KAFKA_TGV_CLIENT_ID_PRODUCER, KAFKA_TGV_TOPIC

if __name__ == '__main__':

    # Get the schema from the Schema Registry.
    r = requests.get(f"{KAFKA_TGV_SCHEMA_REGISTRY_URL}/subjects/{KAFKA_TGV_TOPIC}-value/versions/latest", auth=(KAFKA_TGV_SCHEMA_REGISTRY_USERNAME, KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD))
    print(r.status_code)
    print(r.text)
    schema_id = r.json()['id']
    schema_str = r.json()['schema']

    # In the production environment, the project_id and the Kafka topic have to be the same.
    # Otherwise, the message will not be stored in the database.
    # In the staging environment (Kafka topic test), arbitrary project_id can be used.
    value = {
        "project_id": "test",
        "application_id": "test",
        "device_id": "test",
        "timestamp": int(time.time() * 1e3),  # time in ms
        "measurements": [
            {
                "measurement_id": "heartbeat",
                "unit": None,
                "value": None,
                "measurement_description": None
            },
            {
                "measurement_id": "light_on",
                "unit": None,
                "value": {
                    "boolean": True
                },
                "measurement_description": None
            },
            {
                "measurement_id": "temperature",
                "unit": {
                    "string": "°C" 
                },
                "value": {
                    "int": 42
                },
                "measurement_description": None
            },
            {
                "measurement_id": "pressure",
                "unit": {
                    "string": "Pa" 
                },
                "value": {
                    "float": 42.0
                },
                "measurement_description": None
            },
            {
                "measurement_id": "message",
                "unit": None,
                "value": {
                    "string": "Hello, World!"
                },
                "measurement_description": None
            }
        ],
        "project_description": None,
        "application_description": None,
        "device_description": None,
        "device_type": None,
        "device_manufacturer": None,
        "device_serial": None,
        "location_id": None,
        "location_description": None,
        "latitude": None,
        "longitude": None,
        "altitude": None
    }

    data = {
        "value": value
        # "value_schema_id": schema_id,
        # "value_schema": schema_str,
        # "records": [
        #    {
        #        "value": value
        #    }
        #]
    }
    
    data = {"value":{"type":"JSON","data":value}}

    r = requests.post(f"{KAFKA_TGV_REST_PROXY_URL}/kafka/v3/clusters/{KAFKA_TGV_CLUSTER}/topics/{KAFKA_TGV_TOPIC}/records", json=data,
                      auth=(KAFKA_TGV_API_KEY, KAFKA_TGV_API_PASSWORD))
    print(r.status_code)
    print(r.text)
