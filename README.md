<img src="images/surf-logo.png" height="80"/><img src="images/the-green-village-logo.png" height="80"/>
<!-- ![SURF logo](images/surf-logo.png) ![The Green Village logo](images/the-green-village-logo.png) -->

# Table of content
- [Table of content](#table-of-content)
- [Example producers/consumers for The Green Village digital platform](#example-producersconsumers-for-the-green-village-digital-platform)
  - [Architecture overview](#architecture-overview)
  - [Data schema](#data-schema)
  - [Production and staging environments](#production-and-staging-environments)
  - [Python producers and consumers](#python-producers-and-consumers)
  - [REST Proxy examples](#rest-proxy-examples)
  - [Dashboards](#dashboards)
- [Producer set-up](#producer-set-up)
  - ["Client side"](#client-side)
    - [Virtual environment](#virtual-environment)
    - [Shell script](#shell-script)
    - [Gitlab](#gitlab)
  - [Server-side](#server-side)
    - [Connecting to the producer server](#connecting-to-the-producer-server)
    - [Getting the producer on the server and running](#getting-the-producer-on-the-server-and-running)
    - [Remaining server set-up](#remaining-server-set-up)
      - [Service producers](#service-producers)
      - [Cron producers](#cron-producers)


# Example producers/consumers for The Green Village digital platform

## Architecture overview

The foundation for the Green Village digital platform are managed enterprise-grade services. The streaming part of the platform consists of a Kafka cluster running in Confluent Cloud. Data from Kafka is stored in InfluxDB Enterprise time series database hosted at SURF. Data from the database can be visualised in dashboards created in Grafana Enterprise hosted in Grafana Cloud. 

![The architecture of The Green Village digital platform](images/tud_iot_2023_Q3.png)

| Service | URL |
|---|:---:|
| Kafka cluster in Confluent Cloud | pkc-e8mp5.eu-west-1.aws.confluent.cloud:9092 |
| Schema Registry in Confluent Cloud | https://psrc-1w11j.eu-central-1.aws.confluent.cloud |
| Kafka REST Proxy in Confluent Cloud | https://pkc-e8mp5.eu-west-1.aws.confluent.cloud:443 |
| Grafana Cloud | https://thegreenvillage.grafana.net/ |

The Kafka cluster in Confluent Cloud can be accessed with an API key and a API secret. The same API key and API secret can be used for the Kafka REST Proxy. The Schema Registry can be accessed with a read-only account credentials.
In order to run the examples below, make sure the following environment variables are set in your terminal.

```bash
export KAFKA_TGV_BOOTSTRAP_SERVERS=pkc-e8mp5.eu-west-1.aws.confluent.cloud:9092
export KAFKA_TGV_SCHEMA_REGISTRY_URL=https://psrc-1w11j.eu-central-1.aws.confluent.cloud
export KAFKA_TGV_REST_PROXY_URL=https://pkc-e8mp5.eu-west-1.aws.confluent.cloud:443

export KAFKA_TGV_API_KEY=
export KAFKA_TGV_API_SECRET=
export KAFKA_TGV_CLUSTER=
export KAFKA_TGV_SCHEMA_REGISTRY_USERNAME=
export KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD=

# Topic used as a staging environment
export KAFKA_TGV_TOPIC=tud_gv_test

# Your unique consumer group has to be prefixed with your service account name.
# For example, for service account name "tud_gv_david_salek" the consumer group can be "tud_gv_david_salek_test"
export KAFKA_TGV_CONSUMER_GROUP=
```

## Data schema

The Green Village digital platform only accepts data in a uniform format. The data format is designed to accomodate various kinds of sensor measurements together with the information about the data source. The following fields are defiend in the schema:

| Field | Type | Description |
|---|---|---|
| project_id | metadata | Unique ID for the project; same as the Kafka topic without the tud_gv prefix. |
| application_id | metadata |  Unique ID for a sub-system within the project, if not applicable, make the same as device_id (e.g. within Office Lab is an HVAC system, which consists of measurement devices. “HVAC” would be the application_id).|
| device_id | metadata | Unique ID for the measurement device. |
| project_description (optional) | metadata | Short description/additional information about the project. Can be used for backward traceability. |
| application_description (optional) | metadata | Application/use case description/additional information. Can be used for backward traceability. |
| device_description (optional) | metadata | Device description/additional information about the device. Can be used for backward traceability. |
| device_manufacturer (optional) | metadata | Device manufacturer. |
| device_type (optional) | metadata | Device type. |
| device_serial (optional) | metadata | Device serial number. |
| location_id (optional) | metadata | Unique ID for the location (e.g. DreamHus, Hitteplein, Office Lab, etc). |
| location_description (optional) | metadata | Description of specific measurement location such that the device can be found. |
| timestamp | data| Timestamp (milliseconds since unix epoch) of the measurement. |
| latitude (optional) | data |Latitude in decimal degrees of the device location (GPS coordinates). This is for stationary devices only (moving devices should place the coordinates into the data section). |
| longitude (optional) | data | Longitude in decimal degrees of the device location (GPS coordinates). This is for stationary devices only (moving devices should place the coordinates into the data section). |
| altitude (optional) | data | Altitude in meters above the mean sea level. This is for stationary devices only (moving devices should place the coordinates into the data section). |
| measurements || List of the measurements in the format as described below. In principle, devices can have multiple sensors and thus measure multiple quantities. |

| Field | Type | Description |
|---|---|---|
| measurement_id | metadata | Unique ID for the measurement, especially important when the device measures multiple values. |
| measurement_description (optional) | metadata |Measurement description/additional information. Can be used for backward traceability. |
| unit (optional) | metadata | Unit of the measurement. |
| value (optional) | data | Measured value. |

Each project at The Green Village is required to use its own `project_id`. Every sensor measurement is uniquely defined by a set of `project_id`, `application_id`, `device_id` and `measurement_id`. Data consists of time, place and value.

In principle, different metadata can be sent for a sensor with a unique set of `project_id`, `application_id`, `device_id` and `measurement_id` over the time. This is allowed because the device can be updated (e.g. with a new serial number in case it is replaced, or a device can be moved to a different place). However, it should be avoided to modify metadata too often. By design, metadata should hold static information about the device, and the information changing in time should go into the data section.

The data schema is stored in the Schema Registry, and the messages produced to/consumed from Kafka topics need to be serialized/deserialized in the avro format using this schema.

## Production and staging environments

Each project at The Green Village makes use of its own isolated production environment. This environment can be accessed through a topic in Kafka related to the project. The name of this topic corresponds to the value of `project_id` given to the project, prefixed with `tud_gv_`.

There is one common staging environment to share among all projects. The staging environment can be access through a topic in Kafka called `tud_gv_test`.

The projects are required to use the staging environment first and perform a data quality check together with The Green Village contact person before moving into the production environment. The data quality check is there to guarantee the correct data/metadata structure as well as to confirm the credibility of the data values. The data quality check is an important part of the workflow since data cannot be removed from Kafka.

## Python producers and consumers

Example producers and consumers in Python are available here.
Using producers/consumers in Python (or other languages) is a recommended way for production use.

You can run the examples from a terminal directly or using docker.
Make sure to choose a unique consumer group id for the consumer.

```bash
# Create a python virtual environment (optional)
python -m venv venv
. venv/bin/activate

pip install -U -r requirements.txt

# Run one of the consumer scripts.
python deserializing-consumer.py

# In another terminal, run one of the producer scripts.
# The new message will be printed out by the running consumer script.
python serializing-producer.py
```

```bash
docker build -t ccloud-green-village .

# Consume messages from the topic in one terminal.
docker run -e KAFKA_TGV_BOOTSTRAP_SERVERS=$KAFKA_TGV_BOOTSTRAP_SERVERS -e KAFKA_TGV_API_KEY=$KAFKA_TGV_API_KEY -e KAFKA_TGV_API_SECRET=$KAFKA_TGV_API_SECRET -e KAFKA_TGV_TOPIC=$KAFKA_TGV_TOPIC \
  -e KAFKA_TGV_SCHEMA_REGISTRY_URL=$KAFKA_TGV_SCHEMA_REGISTRY_URL -e KAFKA_TGV_SCHEMA_REGISTRY_USERNAME=$KAFKA_TGV_SCHEMA_REGISTRY_USERNAME -e KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD=$KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD \
  ccloud-green-village python deserializing-consumer.py

# Send a test message to the topic in another terminal.
docker run -e KAFKA_TGV_BOOTSTRAP_SERVERS=$KAFKA_TGV_BOOTSTRAP_SERVERS -e KAFKA_TGV_API_KEY=$KAFKA_TGV_API_KEY -e KAFKA_TGV_API_SECRET=$KAFKA_TGV_API_SECRET -e KAFKA_TGV_TOPIC=$KAFKA_TGV_TOPIC \
  -e KAFKA_TGV_SCHEMA_REGISTRY_URL=$KAFKA_TGV_SCHEMA_REGISTRY_URL -e KAFKA_TGV_SCHEMA_REGISTRY_USERNAME=$KAFKA_TGV_SCHEMA_REGISTRY_USERNAME -e KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD=$KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD \
  -e KAFKA_TGV_CONSUMER_GROUP=$KAFKA_TGV_CONSUMER_GROUP \
  ccloud-green-village python serializing-producer.py
```

## REST Proxy examples

Kafka REST Proxy provides an HTTP interface for interacting with the Kafka cluster. It is useful for testing purposes or for sending infrequent data.

An example of producing data to the Kafka REST Proxy from a command line is available in the rest-produce shell script. Make sure to run init.sh first, so that the rest-produce script can access the necessary variables.

```bash
# Send a test message to the topic.
. init.sh
./rest-produce.sh
```

A similar example is given in Python.

```bash
# Create a python virtual environment (optional)
python -m venv venv
. venv/bin/activate

pip install -U -r requirements.txt

python rest-produce.py
```

```bash
docker build -t ccloud-green-village .

# Send a test message to the topic.
docker run -e KAFKA_TGV_REST_PROXY_URL=$KAFKA_TGV_REST_PROXY_URL -e KAFKA_TGV_API_KEY=$KAFKA_TGV_API_KEY -e KAFKA_TGV_API_SECRET=$KAFKA_TGV_API_SECRET -e KAFKA_TGV_TOPIC=$KAFKA_TGV_TOPIC \
  -e KAFKA_TGV_SCHEMA_REGISTRY_URL=$KAFKA_TGV_SCHEMA_REGISTRY_URL -e KAFKA_TGV_SCHEMA_REGISTRY_USERNAME=$KAFKA_TGV_SCHEMA_REGISTRY_USERNAME -e KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD=$KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD \
  ccloud-green-village python rest-produce.py
```

## Dashboards

All messages sent in the correct data format are stored in the InfluxDB time series database and can be visualised in dashboards in Grafana. 

https://thegreenvillage.grafana.net/dashboards

Public dashboards can be found here:

https://thegreenvillage.grafana.net/public-dashboards/e800334f814e4f8f9650fcc511924aa2?orgId=1

https://thegreenvillage.grafana.net/public-dashboards/c6c3d5130739478d82e79b51961908cb?orgId=1

https://thegreenvillage.grafana.net/public-dashboards/b9fe6e4f8c934041be6658729e125b6e?orgId=1

# Producer set-up
## "Client side"
### Virtual environment
 **The server that currently runs all producers is on python 3.10.12 as of 09/12/2024.**
For the producers in use at The Green Village (TGV) we use virtual environments, as the server where the producers run on uses those so follow these steps to create one locally.

To create a virtual environment you can run the following command: `python -m venv env`, this makes a virtual environment that has the name: "env". This environment is completely seperate from the rest of your system, that means you need to install modules such as: "numpy, the tgv-functions, etc." on the virtual environment. You do this by using a `requirements.txt` file.

A `requirements.txt` file looks somewhat like the following example:

    avro==1.11.3
    certifi==2023.7.22
    charset-normalizer==3.3.0
    confluent-kafka==2.2.0
    fastavro==1.8.3
    idna==3.4
    requests==2.31.0

To add additional modules the format is always: `modulename==version`. 
To install these modules you need to run the following commands:

    . env/bin/activate #watch the space between the . and env
    pip install -r requirements.txt #Only works if the requirements.txt is in the same file otherwise change to relativepathing

To check if the modules are installed correctly you can run the following command: `pip freeze`.
If you follow all these steps you should be able to run the producer on the server.

### Shell script
To run the producer we use a shell script, this script works in tandem with the TGV functions.
The standard shell script for our producers is as follows:

    #!/bin/bash
    source ~/producers/.kafka_envs

    cd ~/producers/producer_name
    . env/bin/activate
    python producer_name.py

The first line in this small script specifies the interpreter, the second line sources the kafka information from somewhere in the system. You need to initialize them somewhere for the TGV functions, they contain the following information:

    KAFKA_TGV_API_KEY
    KAFKA_TGV_API_PASSWORD
    KAFKA_TGV_CONSUMER_GROUP
    KAKFA_TGV_BOOTSTRAP_SERVERS
    KAFKA_TGV_SCHEMA_REGISTRY_URL
    KAFKA_TGV_SCHEMA_REGISTRY_USERNAME
    KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD
    KAFKA_TGV_REST_PROXY_URL

The third line of code changed the directory to te correct location, the fourth line starts the virtual environment (**You need to already have the modules installed on the venv**), the fifth and final line runs the producer, in this case one written in Python. You can save this file with whatever name you want, but most often we save it as `producer_name.sh`.

### Gitlab
When you are done make sure you have pushed all your code to GitLab.
## Server-side
### Connecting to the producer server
For first setup you need someone with admin rights to help you through the process.

The first step is to connect to TU Delft bastion through Cmd (on Windows) or something of the sort, it is suggested you setup a ssh config file for this.
SSH config files can be found/ created in windows under `.ssh/config`, the format to add something to this file is as follows:

    Host ShortenedName
      HostName ConnectingAdress
      User Username
Once you are connected to TU Delft bastion you can proceed to connect to the dataplatform, once again it is recommended to add this to your `.ssh/config` this time in the bastion server. In this config file you should have two different connections, one for your personal account, another for the TGV_Admin account.

In general you should always connect to TGV_Admin in case you can't. Then you can connect using your personal connection and navigate to the home directory using `cd /home`, in this file you can run the command `sudo su tgv-admin`, and then `cd /tgv-admin`.

### Getting the producer on the server and running
Once you are on the server as "tgv-admin" you can start with getting your producer on the server, first start by navigating to the correct file location, in general you can do this by using the following command `cd producers/`. In some cases producers are stored in subdirectories, generally you store your producer on the same spot you have it in the GitLab. 
Once you have found the location you want to put your producer, then in GitLab navigate to the location of your producer, at the topright there is a button with **Code** and a downwards arrow. When you press this a menu will pop-up, copy the URL of section with header `Clone with SSH`, it will be in the form of "git@gitlab.com:the-green-village/projects/REMAININGURL".

Once you have the URL copied you can go back to the location you want to store your producer and run the follow command `git clone git@gitlab.com:the-green-village/projects/REMAININGURL producer_name-location` this will create a new subdirectory with the git cloned. In generaly this directory will contain one or two template files, it will always contain `projectsecrets.py.template`, sometimes it will also contain `config.py.template`. The second one appears mostly when producers are used multiple times. To change these into none template files run the following commands `cp projectsecrets.py.template projectsecrets.py` and `cp config.py.template config.py`. 

Your directory will now contain two or more files you will have to edit. To do this you can run the following command `vim projectsecrets.py`, then you can pres `i` to change into insert mode, so that you can edit the document. Finally you can close and save the document by going through the following steps, press `esc` to get out of insert mode, then type `:w` to save what you have written, after that you can close the document by typing `:q`, you can combines these last two commands by writing `:wq`. The second file you will always need to edit is `producer_name.sh`, to do this you can follow the procedure described as follows, most often you need to adjust the filepaths due to discrepanties between the filepaths of the server and your own system.

Before you can run the producer by using `./producer_name.sh` you need to create a virtual environment, this is described in the section [Virtual environment](#virtual-environment) under "Client-side". Now you can finally run the producer with `./producer_name.sh`, and if everything has been set-up correctly you will see data in the staging environment.

Important is that the file `producer_name.sh` is allowed to execute, you can check this by navigating to the file location and running the command `ls -la`, this will give an example response as follows:

  -rw-rw-r-- 1 tgv-admin tgv-admin   565 Mar  3 17:16 config.py
  -rw-rw-r-- 1 tgv-admin tgv-admin   319 Mar  3 17:15 config.py.template
  drwxrwxr-x 6 tgv-admin tgv-admin  4096 Mar  3 17:19 env
  drwxrwxr-x 8 tgv-admin tgv-admin  4096 Mar  3 17:30 .git
  -rw-rw-r-- 1 tgv-admin tgv-admin    80 Mar  3 17:15 .gitignore
  -rw-rw-r-- 1 tgv-admin tgv-admin   697 Mar  3 17:17 projectsecrets.py
  -rw-rw-r-- 1 tgv-admin tgv-admin   395 Mar  3 17:15 projectsecrets.py.template
  -rw-rw-r-- 1 tgv-admin tgv-admin  3732 Mar  3 17:15 README.md
  -rw-rw-r-- 1 tgv-admin tgv-admin   181 Mar  3 17:15 requirements.txt
  -rw-rw-r-- 1 tgv-admin tgv-admin 15905 Mar  3 17:19 producer_name.py
  -rwxrwxr-x 1 tgv-admin tgv-admin   135 Mar  3 17:29 producer_name.sh

For the producer to run it is important that the line containing `producer_name.sh` has the prefix `-rwxrwxr-x`, if the prefix is `-rw-rw-r--` you can modify the executable right using the following command `sudo chmod +x producer_name.sh`. This allows for the producer to be run using either a Service or Cron as explained below.

### Remaining server set-up
Now that you have your producer running on the server you arent finished yet, first navigate to the logs by using the following command `cd log/`. Here you need to make a new log file, you can do this by running `vim producer_name.log`, and the saving the file by typing `:wq`.

To complete the remaining set-up you need to continue as 'root' instead of as 'tgv-admin', you can exit 'tgv-admin' by writing `exit`. To change to 'root' write `sudo su`.Then do the following command `sudo su` to change your user to 'root'. Then you navigate to the correct directory by writing using the following command `cd /etc/systemd/system`. Then there are two types of producers, 1. the producers that run continously they are run through 'service', and 2. producers that are run every once in a while through the use of 'cron'.

#### Service producers
To setup a producer by using service, you first need to make a new `.service` file in the directory: `/etc/systemd/system`. 

    [Unit]
    Description= DESCRIPTION
    After=multi-user.target
    StartLimitIntervalSec=1

    [Service]
    Type=idle
    Restart=always
    User=tgv-admin
    ExecStart=#LOCATION OF PRODUCER
    StandardError=append:#LOCATION OF LOG

    [Install]
    WantedBy=multi-user.target

However, as all files follow this setup it is easier to copy one of the existing service files and change it, to do so you can run `cp existing_producer.service producer_name-location.service`, then you can change the 'description', 'ExecStart', 'StandardError' to fit. 

Now all the files are set-up we need to turn make it so that the producer actually runs, the first step in this is to run the following command `sudo systemctl status producer_name-location.service`. If you have not done anything the following will be returned:

    ○ producer_name-location.service - Description
        Loaded: loaded (/etc/systemd/system/producer_name-location.service; disabled; vendor preset: enabled)
        Active: inactive (dead)

Then you can run the following command to enable the producer `sudo systemctl enable producer_name-location.service`, if you run the status command again you get the following message:

    ○ producer_name-location.service - Description
        Loaded: loaded (/etc/systemd/system/producer_name-location.service; enabled; vendor preset: enabled)
        Active: inactive (dead)
You can see in the loaded line, that the service is now enabled. 

Now, the final step is to actually start the producer, you do this by running `sudo systemctl start producer_name-location.service`, if you run the status command again you get the following message:

    ● producer_name-location.service - Description
        Loaded: loaded (/etc/systemd/system/producer_name-location.service; enabled; vendor preset: enabled)
        Active: active (running) since DAY YYYY-MM-DD HH:MM:SS CET; Xs ago
      Main PID: PID_NUMBER (PROCESS_NAME)
          Tasks: 29 (limit: 9352)
        Memory: 30.1M
            CPU: 295ms
        CGroup: /system.slice/producer_name-location_.service
                ├─PID_NUMBER /bin/bash /home/tgv-admin/producers/producer_name-location/producer_name.sh
                └─PID_NUMBER python producer_name.py

Now you can check Grafana to see if the producer is actually producing.
#### Cron producers
To set-up a producer using Cron first make sure you are on the server as the correct user, tgv-admin.
Then run the following command `crontab -e', this will open a file where you can run a file at a specific time.

* * * * * ~/producers/SPECIFIC_FILE_PATH/producer_name.sh > ~/log/producer_name.log 2>&1

The stars correspond to the following: * * * * *; minute,  hour, day(month), month, day(week). You can use [Crontab Guru](https://crontab.guru/) (link: https://crontab.guru/) to check.