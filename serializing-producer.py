from confluent_kafka import Producer
from confluent_kafka.serialization import StringSerializer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import SerializationContext, MessageField

import requests
import time
#from pprint import pformat

# Define headers, schema conf etc. Do not change variables here but change in projectsecrets.py. The projectsecrets.py file should be in the code folder.
from projectsecrets import KAFKA_TGV_API_KEY, KAFKA_TGV_API_PASSWORD, KAFKA_TGV_BOOTSTRAP_SERVERS, KAFKA_TGV_SCHEMA_REGISTRY_URL, KAFKA_TGV_SCHEMA_REGISTRY_USERNAME, KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD, KAFKA_TGV_CONSUMER_GROUP, KAFKA_TGV_CLIENT_ID_PRODUCER, KAFKA_TGV_TOPIC

if __name__ == '__main__':

    # Start Kafka producer
    conf = {
    	'url': KAFKA_TGV_SCHEMA_REGISTRY_URL,
    	'basic.auth.user.info': f"{KAFKA_TGV_SCHEMA_REGISTRY_USERNAME}:{KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD}"
    }
    schema_registry_client = SchemaRegistryClient(conf)
    r = requests.get(f"{KAFKA_TGV_SCHEMA_REGISTRY_URL}/subjects/{KAFKA_TGV_TOPIC}-value/versions/latest", auth=(KAFKA_TGV_SCHEMA_REGISTRY_USERNAME, KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD))
    schema_str = r.json()['schema']
    conf = {
            'auto.register.schemas': False
            }
    avro_serializer = AvroSerializer(schema_registry_client,
                                    schema_str,
                                    conf=conf)
    ser_context = SerializationContext(KAFKA_TGV_TOPIC, MessageField.VALUE)

    # Create the producer instance
    producer = Producer(
        {
            'client.id': KAFKA_TGV_CLIENT_ID_PRODUCER,
            'bootstrap.servers': KAFKA_TGV_BOOTSTRAP_SERVERS,
            'sasl.mechanisms': 'PLAIN',
            'security.protocol': 'SASL_SSL',
            'sasl.username': KAFKA_TGV_API_KEY,
            'sasl.password': KAFKA_TGV_API_PASSWORD,
        }
    )

    delivered_records = 0

    # Optional: on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or failed to deliver (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to TOPIC {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )

    value = {
        "project_id": "test",
        "application_id": "test",
        "device_id": "test",
        "timestamp": int(time.time() * 1000),
        "measurements": [
            {
                "measurement_id": "heartbeat",
                "value": None
            },
            {
                "measurement_id": "light_on",
                "value": True
            },
            {
                "measurement_id": "temperature",
                "unit": "°C",
                "value": 42
            },
            {
                "measurement_id": "pressure",
                "unit": "Pa",
                "value": 42.0
            },
            {
                "measurement_id": "message",
                "value": "Hello, World!"
            }
        ]
    }
            
    avro_value = avro_serializer(value, ser_context)
    producer.produce(topic=KAFKA_TGV_TOPIC, value=avro_value, on_delivery=acked)
    producer.poll(0)

    producer.flush()

    print("{} messages were produced to topic {}!".format(delivered_records, KAFKA_TGV_TOPIC))



