#!/bin/bash

# Get the schema defined for a Kafka topic from the Schema Registry.
export SCHEMA_ID=$(curl -s -u $KAFKA_TGV_SCHEMA_REGISTRY_USERNAME:$KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD \
  $KAFKA_TGV_SCHEMA_REGISTRY_URL/subjects/$KAFKA_TGV_TOPIC-value/versions/latest | jq .id)

# Write a test message in a json format into a file.
# The test message uses the current time for the timestamp and a random number as an example temperature measurement.
cat <<EOF > message.json
{
"value":
    {
	"type": "JSON",
	"data":
	{
                "project_id": "test",
                "application_id": "test",
                "device_id": "test",
                "timestamp": $(date +%s)000,
                "measurements": [
                  {
                    "measurement_id": "heartbeat",
                    "unit": null,
                    "value": null,
                    "measurement_description": null
                  },
                  {
                    "measurement_id": "light_on",
                    "unit": null,
                    "value": {
                      "boolean": true
                    },
                    "measurement_description": null
                  },
                  {
                    "measurement_id": "temperature",
                    "value": {
                      "int": 42
                    },
                    "unit": {
                      "string": "°C"
                    },
                    "measurement_description": null
                  },
                  {
                    "measurement_id": "pressure",
                    "unit": {
                      "string": "Pa"
                    },
                    "value": {
                      "float": 42.0
                    },
                    "measurement_description": null
                  },
                  {
                    "measurement_id": "message",
                    "unit": null,
                    "value": {
                      "string": "Hello, World!"
                    },
                    "measurement_description": null
                  }
                ],
                "project_description": null,
                "application_description": null,
                "device_description": null,
                "device_type": null,
                "device_manufacturer": null,
                "device_serial": null,
                "location_id": null,
                "location_description": null,
                "latitude": null,
                "longitude": null,
                "altitude": null
        }
    }
}
EOF


echo $KAFKA_TGV_REST_PROXY_URL/kafka/v3/clusters/$KAFKA_TGV_KAFKA_CLUSTER/topics/$KAFKA_TGV_TOPIC/records

# Produce a message using Kafka REST Proxy.
curl -S -X POST \
  -H "Content-Type: application/json" \
  -u $KAFKA_TGV_API_KEY:$KAFKA_TGV_API_PASSWORD \
  $KAFKA_TGV_REST_PROXY_URL/kafka/v3/clusters/$KAFKA_TGV_KAFKA_CLUSTER/topics/$KAFKA_TGV_TOPIC/records \
  -d @message.json \
